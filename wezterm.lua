-- Pull in the wezterm API
local wezterm = require("wezterm")

-- This table will hold the configuration.
local config = {}

-- In newer versions of wezterm, use the config_builder which will
-- help provide clearer error messages
if wezterm.config_builder then
	config = wezterm.config_builder()
end

-- Font configurations

config.harfbuzz_features = { "zero", "clig=2", "liga=1" }
config.font = wezterm.font_with_fallback({ "Hack Nerd Font Mono", "Noto Color Emoji" })
config.font_size = 14.0
-- This is where you actually apply your config choices

-- Color configurations
config.color_scheme = "zenburn (terminal.sexy)"
config.inactive_pane_hsb = {
	saturation = 3.9,
	brightness = 4.8,
}

config.exit_behavior_messaging = "None"
config.window_background_opacity = 0.9

config.hide_tab_bar_if_only_one_tab = true

config.keys = {
	{
		key = 'F11',
		action = wezterm.action.ToggleFullScreen,
	},
}


-- and finally, return the configuration to wezterm
return config
